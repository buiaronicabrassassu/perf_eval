function [] = computeStatistics(dataPath,factorValue)

addpath ./confidenceInterval/

run('configVariables')

data = loadData(dataPath);
[path,name,ext] = fileparts(dataPath);
statFilePath=fullfile(path,'statistics.stats');

[fileID, error]=fopen(statFilePath,'w');

modules=fieldnames(data.parsedVectors);

for k=1:numel(modules)
    moduleName=modules{k};
    if(isstruct(data.parsedVectors.(moduleName)))
        statistics=fieldnames(data.parsedVectors.(moduleName));
        for i=1:numel(statistics)
            stat=statistics{i};
            
            repetitionsMeans=structfun(@(vec) mean(vec(:,2)),data.parsedVectors.(moduleName).(stat));
            [statMean,statCI]=getMeanWithCI(repetitionsMeans,CONFIDENCE_LEVEL);
            fprintf(fileID,'%s %s %s mean %f %f\n', factorValue, moduleName, stat, statMean, statCI);
        end
    end
end
fclose(fileID);
end

