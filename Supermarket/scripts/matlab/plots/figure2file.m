% Save a figure in a specified path.
% The file has the same name of the figure name property
% The saved picture has dimention of 1024x768

function [] = figure2file(figure, filePath)

dimentions=[1024 768];

filePath=char(filePath);
figCopy = copy(figure);
set(figCopy,'Position',[0 0 dimentions]);

warning('off', 'all');
mkdir(filePath);
warning('on', 'all');

figName=get(figCopy, 'Name');

fullpath = fullfile(filePath, figName);

saveas(figCopy,fullpath,'png');

end
