function [confidenceInterval] = getConfidenceIntervalQuantiles(values,confidenceLevel)

values=sort(values);
numValues = numel(values);

percentiles = ((1:1:numValues)-0.5)./numValues;
% percentiles = zeros(numValues,1);
% for i = 1:numValues
%     percentiles(i) = (i-0.5)/numValues;
% end

alphaHalf=norminv((1-confidenceLevel)/2);

indexLowCI=floor((numValues.*percentiles)-alphaHalf.*sqrt(numValues.*percentiles.*(1-percentiles)));
indexHighCI=ceil((numValues.*percentiles)-alphaHalf.*sqrt(numValues.*percentiles.*(1-percentiles)))+1;

%we approximate the indices to make them integers
lowCI = values(indexLowCI); 
highCI = values(indexHighCI);

confidenceInterval = [lowCI highCI];

end