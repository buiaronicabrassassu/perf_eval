function [] = ecdfStudy(dataPath,scenarioPath,statisticName,distribution)

addpath ../;
addpath ../plots/;
addpath ../confidenceInterval/;

run('configVariables');

dataPath=char(dataPath);
scenarioPath=char(scenarioPath);
statisticName=char(statisticName);
distribution=char(distribution);

structOfVectors = loadData(dataPath);
vectors = structOfVectors.parsedVectors.Supermarket.(statisticName);

zeroFilter=false;
if(ismember(statisticName,ZERO_FILTER))
    zeroFilter=true;
end

[meanQuantiles,confidenceInterval,percentiles] = getMeanQuantilesAllRepetitions(NUM_PERCENTILES,vectors,zeroFilter);

pd = fitdist(meanQuantiles,distribution);
figureName=strcat('ECDF ',statisticName);
fig = ecdf_plot(meanQuantiles,percentiles,figureName,confidenceInterval,pd);

figure2file(fig,scenarioPath);
end
