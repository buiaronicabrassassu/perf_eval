%The function plot the epdf with the confidence interval and the
%theroretical distribution 'pd' that fit this values.


function fig = ecdf_plot(values,percentiles,figureName,confidenceInterval,pd)

addpath ../;

fig=figure('Name',figureName);
title(figureName);
hold on;
set(fig, 'Visible', 'off');

hold on;
grid on;

e = errorbar(values,percentiles,confidenceInterval,'horizontal');
e.Color = 'r';
e.LineWidth  = 0.01;
e.LineStyle='none';

ecdf = cdfplot(values);
ecdf.Color = 'blue';
y = cdf(pd,values);
teoDist=plot(values,y,'-');
teoDist.LineWidth=2;

legend('Confidence interval','Empirical','Theoretical','Location','southeast');


end
