function [angularCoeff,offset,rSquared,CI] = getLinearRegression(x,y,confidenceLevel)

mdl = fitlm(x,y,'linear');
beta = mdl.Coefficients.Estimate;
rSquared = mdl.Rsquared.ordinary;
angularCoeff = beta(2);
offset = beta(1);

if(nargin == 3)
    CI = coefCI(mdl,1-confidenceLevel);

end

