function [positiveX,transformedY] = logTransform(x,y)

positiveX = x(y > 0);
positiveY = y(y > 0); 
transformedY = log(positiveY);

end

