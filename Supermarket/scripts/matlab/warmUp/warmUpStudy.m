function [] = warmUpStudy(dataPath,scenarioPath,statisticName)
addpath ../;
addpath ../plots/;

run('configVariables')

if(nargin < 3)
    statisticName='queueingTime';
end

data = loadData(char(dataPath));

%select the statistic structure
repetitions = data.parsedVectors.Supermarket.(char(statisticName));

repetitionsFields=fieldnames(repetitions);

yValues={};
xValues={};
for r=1:numel(repetitionsFields)
    repetition = repetitions.(repetitionsFields{r});
    
    xValues{r} = repetition(:,1);
    
    switch WARMUP_MEAN_MODE
        case 'moveMean'
            yValues{r} = movmean(repetition(:,2),WINDOW_SIZE);
        case 'incrementalMean'
            yValues{r} = incrementalMeen(repetition(:,2));
    end
end

figureName=strcat(statisticName,strcat(statisticName,'WarmUp'));
figure2file(warmUpPlot(xValues, yValues, figureName), scenarioPath);

end
