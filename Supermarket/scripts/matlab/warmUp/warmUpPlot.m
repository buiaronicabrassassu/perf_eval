%data is a sctucture with the current format:
%data{
%   rep_0: [times,values]
%   .
%   .
%}

function fig = warmUpPlot(xValues, yValues, figureName)

fig=figure('Name', figureName);

set(fig,'Visible','off');

hold on;
grid on;
for row=1:size(xValues,2)
    plot(xValues{row},yValues{row});
end

%ylim([0 20]);
hold off;

end
