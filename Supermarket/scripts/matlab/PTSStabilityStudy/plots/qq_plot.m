function [ qq_figure ] = qq_plot(values,figure_name)

qq_figure=figure('Name',figure_name);
title('Residuals/Normal QQ Plot');

set(qq_figure, 'Visible', 'off');

h = qqplot(sort(values(:)));

grid on;
h(1).Marker='*';
qq_figure.CurrentAxes.XAxisLocation = 'origin';
qq_figure.CurrentAxes.YAxisLocation = 'origin';

end
