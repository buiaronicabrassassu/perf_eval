% Computes 2kr factors_weight, SSX, SSE, qi, residuals and CI 
% valuse is a 2^(#factors) x R matrix, 
% R , r-> the number of repetitions. 
% alpha-> desired confidence, (possible values are: 0.95 0.99...)

function [factors_weight, CI,  qi, SSX, SSE, mean_values, residuals ] = analisys_2kr( coeff, values ,alpha)

extended_coeff = extended_2kr_coeff_matrix(coeff);

n_factors = size(coeff,2);
n_factors_combs= size(extended_coeff,2);    
r = size(values, 2);                        

mean_values =  mean(values,2);
residuals = (values - mean_values);
SSE = sum(sum(residuals.^2));

SSX = zeros(n_factors_combs,1);
qi= zeros(n_factors_combs,1);

for i=1:n_factors_combs
    qi(i) = dot(extended_coeff(:,i),mean_values)/(2^n_factors);
end

SSX = (qi.^2).*r.*(2^n_factors);
SST = sum([SSX; SSE]);

factors_weight = [SSX./SST; SSE/SST].*100;


freedom_deg = (2^n_factors)*(r-1);
residual_variance = SSE/((r-1)*(2^n_factors));
CI = abs(tinv((1-alpha)/2,freedom_deg)*sqrt(residual_variance/((2^n_factors) * r)));
end

