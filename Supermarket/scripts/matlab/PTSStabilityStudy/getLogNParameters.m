function [logMean, logStd] = getLogNParameters(distMean, distStd)

distVar=distStd^2;
varOverMeanSquare=distVar/(distMean^2);

logMean=log(distMean/sqrt(1 + varOverMeanSquare));
logStd=sqrt(log(1+varOverMeanSquare));

end

