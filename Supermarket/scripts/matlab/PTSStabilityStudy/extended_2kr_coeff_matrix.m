% Coeff is a 2^(# factors)x(# factors) matrix.
% this fanction extends the coeff matrix with all the possible combinations
% between factors

function [ extended_coeff ] = exteded_2kr_coeff_matrix( coeff )

factors_number = size(coeff, 2);
column_size=size(coeff,1);

extended_coeff = coeff;

factor_indexes = 1:1:factors_number;


for i=2:factors_number
    %compute all combs of i elements 
    index_combs = combnk(factor_indexes,i);
    add_col = ones(column_size,1);
    
    for j=1:size(index_combs,1)
        for k=1:size(index_combs,2)
            index = index_combs(j,k);
            add_col= add_col.*coeff(:,index);
        end
    end
    extended_coeff = [extended_coeff add_col];
end

