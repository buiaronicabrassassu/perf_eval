function [serviceTimes] = computeMeanServiceTimeLogN()

distMean=120;
distStd=65;

[logMean, logStd]=getLogNParameters(distMean, distStd);

objServiceTime=3;
paymentTime= 30;
objectNumber=5:1:20;

fun = @(x) x.*exp((-1.*((log(x)-logMean).^2))./(2.*(logStd.^2)))./(x.*sqrt(2.*pi).*logStd); 

fastTillthresholds=paymentTime+(objectNumber*objServiceTime);

fastTillprobabilities = logncdf(fastTillthresholds,logMean,logStd);
standardTillprobabilities = 1-fastTillprobabilities;

indexes=1:1:numel(fastTillthresholds);

ftst=arrayfun(@(i)integral(fun,0,fastTillthresholds(i))./fastTillprobabilities(i),indexes);
stst=arrayfun(@(i)integral(fun,fastTillthresholds(i), Inf)./standardTillprobabilities(i),indexes);

serviceTimes =[objectNumber;ftst ; stst];

end
