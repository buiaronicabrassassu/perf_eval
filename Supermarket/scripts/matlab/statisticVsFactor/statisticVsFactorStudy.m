%structOfVectors:{
% [statisticName]:{
%        [factor_value]:{
%           factor: [factor_value]
%     [statType]: [vector of values]
%       }
%     ...
% }
% ...
%}

function statisticVsFactorStudy(dataPath, scenarioPath, statisticName, chartLegend)
%try
    addpath ../;
    addpath ../plots/;
    addpath ../confidenceInterval/;
    
    run('configVariables');
    
    confidenceLevel = 0.95;
    
    numberOfFunction = numel(dataPath);
    indexes = 1:1:numberOfFunction;
    
    %return a cell array with a data struct for every function to plot
    data = arrayfun(@(i) loadData(char(dataPath(i))),indexes); 
    
    %select the statistic structure for every function to plot
    for i=1:numberOfFunction
        if isstruct(data)
            statisticDataStruct=data(i);
        else
            statisticDataStruct=data;
        end
        if numel(statisticName)==1
            structOfVectors{i} = statisticDataStruct.parsedStatistics.Supermarket.(char(statisticName(1)));
        else
            structOfVectors{i} = statisticDataStruct.parsedStatistics.Supermarket.(char(statisticName(i)));
        end
    end
   
    statisticValues = [];
    confidenceInterval = [];
    
    %scan every structOfVectors
    for i=1:numberOfFunction
        fieldNames = fieldnames(structOfVectors{i});
        newColStatisticValues = [];
        newColConfidenceInterval = [];
        factorValues = [];
        
        factorName='';
        %scan every factor values for the selected function to plot
        for r=1:numel(fieldNames)
            variableDataStruct = structOfVectors{i}.(fieldNames{r});
            if(strcmp(factorName,''))
                vdsFielsNames=fieldnames(variableDataStruct);
                factorName=vdsFielsNames{1};
            end
            
            %the factor values are the same for every function to plot, but is used to reorder the other columns
            factorValues = [factorValues; variableDataStruct.(factorName)(1)];
            newColStatisticValues = [newColStatisticValues; variableDataStruct.(STATISTIC_TYPE)(1)];
            newColConfidenceInterval = [newColConfidenceInterval; variableDataStruct.(STATISTIC_TYPE)(2)];
        end
        
    %order according to factor values    
    temp = sortrows([factorValues newColStatisticValues newColConfidenceInterval],1);
    factorValues = temp(:,1);
    newColStatisticValues = temp(:,2);
    newColConfidenceInterval = temp(:,3);
        
    statisticValues = [statisticValues newColStatisticValues];
    confidenceInterval = [confidenceInterval newColConfidenceInterval];
    end
    
    if(exist('CONSTANT_TO_PLOT','var')==1)
        statisticSize=size(statisticValues,1);
        statisticValues = [statisticValues ones(statisticSize,1).*CONSTANT_TO_PLOT(1)];
        confidenceInterval = [confidenceInterval ones(statisticSize,1).*CONSTANT_TO_PLOT(2)];
        chartLegend = [chartLegend CONSTANT_TO_PLOT_STAT_NAME];
    end    
    fig1 = plotStatisticVsFactor(factorValues,statisticValues,confidenceInterval,statisticName(1)+'_vs_'+factorName,chartLegend);
    figure2file(fig1,scenarioPath);
    
    
%catch
%    clear;
%end
end
