function [fig] = plotStatisticVsFactor(factorValues,statisticValues,confidenceInterval,figureName,chartLegend)

fig=figure('Name', figureName);
title(strrep(figureName,'_',' '));
set(fig, 'Visible', 'off');

hold on;
grid on;

numberOfFunction = size(statisticValues,2);
for i=1:numberOfFunction
    e = errorbar(factorValues,statisticValues(:,i),confidenceInterval(:,i),confidenceInterval(:,i));
    e.MarkerEdgeColor='red';
    e.MarkerSize=5;
    e.Marker = '*';
end

set(gca,'XTick', factorValues );

xlim([0 factorValues(end)*1.2]);
%ylim([0 400]);
legend(chartLegend,'southeast');

hold off;

end
