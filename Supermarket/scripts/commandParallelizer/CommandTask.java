import java.io.*;

public class CommandTask implements Runnable{

    private String command;
    private String argument;

    public CommandTask(String command, String argument)
    {
        this.command=command;
        this.argument=argument;
    }

    public void run() {
        Process p;
        try {
            //System.out.println("Thread execute--> "+command+" "+argument);
            p = Runtime.getRuntime().exec(command+" "+argument);
            //System.out.println("Commad output--> "+p.getErrorStream().toByteArray());
            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";

            while ((line = b.readLine()) != null) {
                System.out.println(line);
            }

b.close();
        }
        catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

}
