import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.nio.*;
import java.nio.file.Paths;


public class CommandParallelizer {

    static void execParallelization(String command, int threadNumber, String[] arguments){
        ExecutorService threadPool = Executors.newFixedThreadPool(threadNumber);
        List<Future> listFuture = new ArrayList<>();
        System.out.println("Command--> "+Paths.get(command).getFileName());
        System.out.println("Arguments number--> "+arguments.length);
        for(String argument: arguments)
        {
            listFuture.add(threadPool.submit(new CommandTask(command,argument)));
        }

        listFuture.forEach((Future f)->{
            try {
                f.get();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        threadPool.shutdown();
    }


    public static void main(String[] args) {
        final String commandName=args[0];
        final String commandOpt=args[1];
        final String command = commandName+" "+commandOpt;
        final int threadNumber = Integer.parseInt(args[2]);
        final String[] arguments = Arrays.copyOfRange(args, 3, args.length);

        execParallelization(command,threadNumber,arguments);
    }

}
