#awk -v out=[output_file_name].m -f [awk_script] [pat_to_.vec_files]/*.vec
#
#The script create a structure whit this format:
#
#data:{
#	[statisticName]:{
#		rep_n:[times,values]
#		.
#		.
#	}
#	.
#	.
#}
#The script parse only .vec file


#Setting up .m file
BEGIN{
    print "parseVec:\tInitialization"
    out_file = out".m"
    print "data = struct;" > out_file
    number_of_repetitions = 0
    current_repetition = 0
}

#get current run number
/attr runnumber/{
    current_repetition = $3
    number_of_repetitions = number_of_repetitions + 1;

    print "parseVec:\tParsing repetition "current_repetition
}

#initialize a structure for each statistics and for each repetition
/vector [0-9]+/{
     dim = split($4,splitted,":")
     vector_stat = splitted[1]

     moduleName=gensub(/\]/,"","g",gensub(/(\.|\[)/,"_", "g", $3))

     if(length(struct[moduleName])==0){
         struct[moduleName]= "data."moduleName"= struct;"
         print struct[moduleName] > out_file
     }
     #if the statistic structure isn't defined yet, define it
     if(length(struct[moduleName,vector_stat])==0){
         struct[moduleName,vector_stat]= "data."moduleName"."vector_stat"= struct;"
         print struct[moduleName,vector_stat] > out_file
     }

     #create the current repetition array into the current statistic structure
     vectors[$2,current_repetition]="data."moduleName"."vector_stat".rep_"current_repetition"=["

}

#fill the current repetiotion vector
/^[0-9]+/{
    vectors[$1,current_repetition] = vectors[$1,current_repetition] $3" "$4";"
}

#complete each vector and print them into the output file
END{
    print "parseVec:\tAll parse completed. Creation of "out_file
    for(i in vectors){
        vectors[i] = vectors[i]"];"
        print vectors[i] > out_file
    }

    print "data.repetition_number = " number_of_repetitions";" > out_file

    print "parseVec:\t"out_file" created. Script completed"
}
