#parameters out="out_file_name" factor="factor_value"
#the script parse the .sca files
#each row of the output file will have the current format:
#[factor_value] [moduleName] [statisticName] [statistic] [value] [value] ......

BEGIN{
    print "parseStat: Start parsing scenario " factor
    FS = "\n"
    #RS = "statistic Supermarket[\t| ]+"
    RS = "statistic"

    out_file = out".stats"
}

#get current run number
#/attr runnumber/{
    #dim= split($15,fieldRows," ")
    #repetition = fieldRows[3]
#}

/:stats/{
    rowsDim = split($0,fieldRows,/\n/)

    #parsing a row with this format: Supermarket.standardTills[0] 	responseTime:stats
    fieldsDim=split(fieldRows[1],fields," ")
    #delete :stats or similar from the satistic name
    statisticName=gensub(/:.+/, "", 1, fields[2]);
    #in Supermarket.standardTills[0] delete the ] and change . or [ with _
    moduleName=gensub(/\]/,"","g",gensub(/(\.|\[)/,"_", "g", fields[1]))

    #parse the mean statistic row
    fieldsDim=split(fieldRows[3],fields," ")
    statisticType=fields[2]
    statisticValue = fields[3]

    if(length(data[moduleName,statisticName])==0){
         data[moduleName,statisticName]=factor " " moduleName " " statisticName " " statisticType
     }
    data[moduleName,statisticName] = data[moduleName,statisticName] " " statisticValue
}

END{
    for(i in data){
        print data[i] > out_file
    }

    print "parseStat: parse scenario " factor " ended"
}
