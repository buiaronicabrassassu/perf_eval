#!/bin/bash
#Script parameters:
#-scenario name (the scenario name must be the same specified in the omentpp.ini)
CURRENT_SCRIPT_FOLDER_PATH=$(readlink -f $(dirname $0))
CURRENT_SCRIPT_NAME=$(basename $0)
PROJECT_PATH=$(echo $CURRENT_SCRIPT_FOLDER_PATH | awk '{split($1,path,"/Supermarket"); print path[1];}')
SUPERMARKET_PATH=$PROJECT_PATH/Supermarket
SIMULATIONS_PATH=${SUPERMARKET_PATH}/simulations
SCRIPTS_PATH=${SUPERMARKET_PATH}/scripts
AWK_PATH=${SCRIPTS_PATH}/awk

SCENARIO_FOLDER=""
INI_FILE_NAME="queueingTimeStudy.ini"
VALGRIND_MODE=0

while getopts m:w:vh option; do
 case ${option} in
 m) SCENARIO_FOLDER=$(readlink -f $OPTARG)
 SCENARIO="Measurament";;
 w) SCENARIO_FOLDER=$(readlink -f $OPTARG)
 SCENARIO="WarmUp";;
 v) VALGRIND_MODE=1;;
 h)
 echo -e "\nAvailable options are:"
 echo "-m [scenario folder path]: run Measurament scenario"
 echo "-w [scenario folder path]: run WarmUp scenario"
 echo "-v: run simulation using valgrind"
 exit 1;;
 \?)
    exit 1;;
 esac
done

if [ "${SCENARIO_FOLDER}" == "" ]
  then
    echo -e "$CURRENT_SCRIPT_NAME: ERROR"
    echo -e "\tThe simulation scenario folder path must be specified."
    echo -e "\tUse -h to print help."
    exit -1;
fi

echo "$CURRENT_SCRIPT_NAME: INIT"

SCENARIO_FOLDER_NAME="$(basename $SCENARIO_FOLDER)"
SCENARIO_INI_FILE=${SCENARIO_FOLDER}/${INI_FILE_NAME}

#Compute all the factors values of the scenario
FACTOR_VALUE=$(echo ${SCENARIO_FOLDER_NAME}|awk '{print gensub(/[A-Za-z]+_/,"","g",$0);}')

echo -e "$CURRENT_SCRIPT_NAME:\tDELETE ALL RESULTS FILES"
${CURRENT_SCRIPT_FOLDER_PATH}/delete_files -vVsSmM -f ${SCENARIO_FOLDER} > /dev/null

#run the simulation
if [ $VALGRIND_MODE -eq 1 ]
then
    echo -e "$CURRENT_SCRIPT_NAME:\t(VALGRIND MODE)RUN SIMULATION $SCENARIO_FOLDER_NAME IN ${SCENARIO} MODE"
    ${SIMULATIONS_PATH}/valgrind_run -f ${SCENARIO_INI_FILE} -c ${SCENARIO}  > /dev/null
else
    echo -e "$CURRENT_SCRIPT_NAME:\tRUN SIMULATION $SCENARIO_FOLDER_NAME IN ${SCENARIO} MODE"
    ${SIMULATIONS_PATH}/run -f ${SCENARIO_INI_FILE} -c ${SCENARIO} > /dev/null
fi


#parse .vec results if exists
vecCount=$(ls -1 ${SCENARIO_FOLDER}/results/*.vec 2>/dev/null | wc -l)
if [ ${vecCount} -gt 0 ]
    then
        echo -e "$CURRENT_SCRIPT_NAME:\tPARSE .VEC RESULTS"
        awk -v out=${SCENARIO_FOLDER}/parsedVectors -f ${AWK_PATH}/parse_vectors.awk ${SCENARIO_FOLDER}/results/*.vec > /dev/null
fi

#parse .sca results if exists
scaCount=$(ls -1 ${SCENARIO_FOLDER}/results/*.sca 2>/dev/null | wc -l)
if [ $VALGRIND_MODE -eq 0 ] && [ ${scaCount} -gt 0 ]
    then
        echo "$CURRENT_SCRIPT_NAME:\tPARSE .SCA RESULTS"
        awk -v out=${SCENARIO_FOLDER}/simulationStats -v factor="${FACTOR_VALUE}" -f ${AWK_PATH}/parse_scalar.awk ${SCENARIO_FOLDER}/results/*.sca > /dev/null
fi

echo "$CURRENT_SCRIPT_NAME: FINISH"
