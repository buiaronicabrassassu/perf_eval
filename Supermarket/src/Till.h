//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SUPERMARKET_TILL_H_
#define __SUPERMARKET_TILL_H_

#include <omnetpp.h>
#include <queue>
#include <vector>
#include <string>
#include "Client_m.h"
/**
 * TODO - Generated class
 */
class Till : public cSimpleModule
{
private:
    int id;
    bool busy;

    std::queue<Client*>* tillQueue;
    cMessage* servedClient;

    std::vector<simsignal_t>* queueingTimeSignals;
    std::vector<simsignal_t>* responseTimeSignals;
    simsignal_t numberOfClient;
  protected:
    std::vector<simsignal_t>* parseSignals(const std::string signals);
    void emitSignals(std::vector<simsignal_t>* signals, const double value);

    virtual void initialize(int stage);
    virtual int numInitStages() const{
            return 1;
        }
    virtual void handleMessage(cMessage *msg);
    virtual void handleNextClient();
    virtual void serveClient(Client *c);
    virtual void handleNewClient(cMessage *msg);
    virtual void notifyServedClient();

    virtual void finish();
  public:
    virtual const int getNumberOfClient();
};

#endif
