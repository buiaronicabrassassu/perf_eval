//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SUPERMARKET_TILLSWITCHER_H_
#define __SUPERMARKET_TILLSWITCHER_H_

#include <omnetpp.h>
#include <vector>
#include "Till.h"
#include "Client_m.h"
/**
 * TODO - Generated class
 */
class TillSwitcher : public cSimpleModule
{
private:
    std::vector<int>* minHeap;
    std::vector<Till*>* tillReferences;

    int numberOfTills;

    class Comparator{
        const TillSwitcher * tillSw;
    public:
        Comparator(const TillSwitcher * t);
        bool operator()(const int&, const int&) const;
    };

protected:
    virtual void initialize(int stage);
    virtual int numInitStages() const{
        return 2;
    }
    virtual void handleMessage(cMessage *msg);
    virtual void createMinHeap();
    virtual void tillSwitching(Client *nextClient);
    virtual void sendNextClient(int gate, Client* nextClient);
    virtual void printTillsStatus();
    virtual void finish();
};

#endif
